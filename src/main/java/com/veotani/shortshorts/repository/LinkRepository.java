package com.veotani.shortshorts.repository;

import com.veotani.shortshorts.entity.ShortLink;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkRepository extends CrudRepository<ShortLink, String> { }
