package com.veotani.shortshorts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShortshortsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShortshortsApplication.class, args);
    }

}
