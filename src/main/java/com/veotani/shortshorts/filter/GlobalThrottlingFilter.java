package com.veotani.shortshorts.filter;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.CacheSimpleConfig;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.config.Config;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICacheManager;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.grid.GridBucketState;
import io.github.bucket4j.grid.RecoveryStrategy;
import io.github.bucket4j.grid.jcache.JCache;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.util.Objects;

public class GlobalThrottlingFilter implements javax.servlet.Filter {

    private static final String BUCKET_ID = "global-limit";

    private javax.cache.Cache<String, GridBucketState> cache;

    private Bucket bucket;

    private final int hezelcastConnectTimout;
    private final String hezelcastHost;
    private final int hezelcastPort;
    private final int throttlingCapacity;
    private final int throttlingDuration;

    /**
     * Connect to existing hezelcast instance
     */
    private void GetExistingCache() {
        NearCacheConfig cacheConfig = new NearCacheConfig();
        cacheConfig.setName(BUCKET_ID);
        ClientConfig config = new ClientConfig();
        config.addNearCacheConfig(cacheConfig);
        config.getNetworkConfig().addAddress(hezelcastHost + ":" + hezelcastPort);
        config.getConnectionStrategyConfig().getConnectionRetryConfig().setClusterConnectTimeoutMillis(hezelcastConnectTimout);
        HazelcastInstance hezelcastClient = HazelcastClient.newHazelcastClient(config);
        ICacheManager cacheManager = hezelcastClient.getCacheManager();
        cache = cacheManager.getCache(BUCKET_ID);
    }

    /**
     * Create subprocess of hezelcast that can be used by another services
     */
    private void CreateCache() {
        Config config = new Config();
        CacheSimpleConfig cacheConfig = new CacheSimpleConfig();
        cacheConfig.setName(BUCKET_ID);
        config.addCacheConfig(cacheConfig);
        config.getNetworkConfig().setPublicAddress(hezelcastHost + ":" + hezelcastPort);
        HazelcastInstance hezelcastInstance = Hazelcast.newHazelcastInstance(config);
        ICacheManager cacheManager = hezelcastInstance.getCacheManager();
        cache = cacheManager.getCache(BUCKET_ID);
    }

    /**
     * Filter creation
     *
     * @param hezelcastHost hezelcast host
     * @param hezelcastPort hezelcast port
     * @param hezelcastConnectTimout how much ms to wait before creating new hezelcast instance
     * @param throttlingCapacity amount of requests the server can process within throttlingDuration
     * @param throttlingDuration throttling period
     */
    public GlobalThrottlingFilter(String hezelcastHost, int hezelcastPort, int hezelcastConnectTimout, int throttlingCapacity, int throttlingDuration) {
        this.hezelcastHost = hezelcastHost;
        this.hezelcastPort = hezelcastPort;
        this.hezelcastConnectTimout = hezelcastConnectTimout;
        this.throttlingCapacity = throttlingCapacity;
        this.throttlingDuration = throttlingDuration;
        try {
            GetExistingCache();
        } catch (Exception e) {
            CreateCache();
        }
    }

    /**
     * Create bucket
     *
     * @param filterConfig filtering configuration
     */
    @Override
    public void init(FilterConfig filterConfig) {
        bucket = Bucket4j.extension(JCache.class).builder()
                .addLimit(Bandwidth.simple(throttlingCapacity, Duration.ofSeconds(throttlingDuration)))
                .build(cache, BUCKET_ID, RecoveryStrategy.RECONSTRUCT);
    }

    /**
     * Filter post requests via throttling policy: allow throttlingCapacity requests to be processed within throttlingDuration
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (!Objects.equals(request.getMethod(), "POST")) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        if (bucket.tryConsume(1)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
            httpResponse.setContentType("text/plain");
            httpResponse.setStatus(429);
            httpResponse.getWriter().append("Too many requests");
        }
    }
}
