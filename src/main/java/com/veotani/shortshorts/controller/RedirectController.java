package com.veotani.shortshorts.controller;

import com.veotani.shortshorts.exceptions.LinkDoesntExist;
import com.veotani.shortshorts.service.LinkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class RedirectController {
    private LinkService linkService;

    @Autowired
    public void setLinkService(LinkService linkService) {
        this.linkService = linkService;
    }

    /**
     * Using short link id redirect to it's full link
     *
     * @param linkId short link id
     * @return redirection response
     */
    @GetMapping("/{linkId}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "302", description = "Short link redirection"),
            @ApiResponse(responseCode = "404", description = "Short link doesn't exist", content = @Content(
                    schema = @Schema(implementation = String.class)
            )),
    })
    @Operation(summary = "Go to full link by short link")
    @CrossOrigin(origins = "*", methods = RequestMethod.GET)
    public RedirectView shortLinkRedirect(@PathVariable String linkId) throws LinkDoesntExist {
        return new RedirectView(this.linkService.getFullLink(linkId));
    }

    @ExceptionHandler(LinkDoesntExist.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleNoSuchElementFoundException(LinkDoesntExist exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(exception.getMessage());
    }
}
