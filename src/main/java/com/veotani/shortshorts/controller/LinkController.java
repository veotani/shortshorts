package com.veotani.shortshorts.controller;

import com.veotani.shortshorts.dto.CreateLinkDto;
import com.veotani.shortshorts.dto.GetFullLinkDto;
import com.veotani.shortshorts.dto.LinkCreatedDto;
import com.veotani.shortshorts.exceptions.LinkDoesntExist;
import com.veotani.shortshorts.service.LinkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/link")
@RestController
public class LinkController {
    private LinkService linkService;

    @Autowired
    public void setLinkService(LinkService linkService) {
        this.linkService = linkService;
    }

    /**
     * Create new short link
     *
     * @param createLinkDto - full link details
     * @return short link id to be used within this service as https://DOMAIN/ID for redirection
     */
    @PostMapping
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "New link is saved"),
            @ApiResponse(responseCode = "429", description = "Too many requests", content = @Content(
                    schema = @Schema(implementation = String.class)
            )),
    })
    @Operation(summary = "Create new short link")
    @CrossOrigin(origins = "*", methods = RequestMethod.POST)
    public LinkCreatedDto post(@RequestBody CreateLinkDto createLinkDto) {
        return new LinkCreatedDto(linkService.createShortLink(createLinkDto.getLink()));
    }

    /**
     * Get full link by short link id
     *
     * @param linkId id of the short link
     * @return full link
     */
    @GetMapping("/{linkId}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Link is found and returned"),
            @ApiResponse(responseCode = "404", description = "Link doesn't exist", content = @Content(
                    schema = @Schema(implementation = String.class)
            )),
    })
    @Operation(summary = "Get full link by it's short link")
    @CrossOrigin(origins = "*", methods = RequestMethod.GET)
    public GetFullLinkDto get(@PathVariable String linkId) throws LinkDoesntExist {
        return new GetFullLinkDto(linkService.getFullLink(linkId));
    }

    @ExceptionHandler(LinkDoesntExist.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleNoSuchElementFoundException(LinkDoesntExist exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(exception.getMessage());
    }
}
