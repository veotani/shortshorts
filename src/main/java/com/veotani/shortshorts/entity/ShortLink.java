package com.veotani.shortshorts.entity;

import lombok.Getter;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash(value = "ShortLink", timeToLive = 600)
@Getter
public class ShortLink implements Serializable {
    private final String id;
    private final String link;

    /**
     * Create short link description (short and full link must be known)
     *
     * @param id short link id
     * @param link full link
     */
    public ShortLink(String id, String link) {
        this.id = id;
        this.link = link;
    }
}
