package com.veotani.shortshorts.service;

import com.veotani.shortshorts.entity.ShortLink;
import com.veotani.shortshorts.exceptions.ImpossibleHash;
import com.veotani.shortshorts.exceptions.LinkDoesntExist;
import com.veotani.shortshorts.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
public class LinkService {
    private final LinkRepository linkRepository;

    @Autowired
    public LinkService(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    /**
     * Create short link by full link
     *
     * @param link full link
     * @return short link entity
     */
    private ShortLink shortenLink(String link) {
        String linkHash = DigestUtils.md5DigestAsHex(link.getBytes(StandardCharsets.UTF_8));
        for (int hashLength = 1; hashLength < linkHash.length(); hashLength++) {
            String shortHash = linkHash.substring(0, hashLength);
            Optional<ShortLink> shortLink = linkRepository.findById(shortHash);
            if (shortLink.isEmpty()) {
                return new ShortLink(shortHash, link);
            }
            if (shortLink.get().getLink().equals(link)) {
                return shortLink.get();
            }
        }
        throw new ImpossibleHash("Hash for link " + link + " can't be calculated");
    }

    /**
     * Create short link by given full link
     *
     * If link already exists, return it without changing its TTL.
     *
     * @param link full link
     * @return short link id
     */
    public String createShortLink(String link) {
        ShortLink shortLink = shortenLink(link);
        if (linkRepository.findById(shortLink.getId()).isPresent()) {
            return linkRepository.findById(shortLink.getId()).get().getId();
        }
        ShortLink savedLink = linkRepository.save(shortLink);
        return savedLink.getId();
    }

    /**
     * Get full link by short.
     *
     * @param linkId short link id
     * @return full link as string
     */
    public String getFullLink(String linkId) throws LinkDoesntExist {
        Optional<ShortLink> shortLink = linkRepository.findById(linkId);
        if (shortLink.isPresent()) {
            return shortLink.get().getLink();
        }
        throw new LinkDoesntExist("Given short link doesn't exist.");
    }
}
