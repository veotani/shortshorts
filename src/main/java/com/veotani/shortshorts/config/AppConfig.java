package com.veotani.shortshorts.config;

import com.veotani.shortshorts.filter.GlobalThrottlingFilter;
import com.veotani.shortshorts.filter.LoggingFilter;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${spring.hezelcast.timeout}")
    private int hezelcastConnectTimout;

    @Value("${spring.hezelcast.host}")
    private String hezelcastHost;

    @Value("${spring.hezelcast.port}")
    private int hezelcastPort;

    @Value("${spring.throttling.capacity}")
    private int throttlingCapacity;

    @Value("${spring.throttling.duration}")
    private int throttlingDuration;

    /**
     * Apply filtering for certain endpoints.
     *
     * @return filter registration bean to be injected into application
     */
    @Bean
    public FilterRegistrationBean<GlobalThrottlingFilter> filterRegistrationBean() {
        FilterRegistrationBean<GlobalThrottlingFilter> registrationBean = new FilterRegistrationBean<>();
        GlobalThrottlingFilter globalThrottlingFilter = new GlobalThrottlingFilter(
                hezelcastHost,
                hezelcastPort,
                hezelcastConnectTimout,
                throttlingCapacity,
                throttlingDuration
        );

        registrationBean.setFilter(globalThrottlingFilter);
        registrationBean.addUrlPatterns("/link");
        registrationBean.setOrder(2);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<LoggingFilter> loggingFilterFilterRegistrationBean() {
        FilterRegistrationBean<LoggingFilter> registrationBean = new FilterRegistrationBean<>();
        LoggingFilter loggingFilter = new LoggingFilter();
        registrationBean.setFilter(loggingFilter);
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }
}
