package com.veotani.shortshorts.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Raise when user tries to use id of not-existing link
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Short link doesn't exist")
public class LinkDoesntExist extends ResponseStatusException {
    public LinkDoesntExist(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
