package com.veotani.shortshorts.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Thrown when hashing function couldn't find hash (collision happened)
 */
@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Couldn't find hash for given link")
public class ImpossibleHash extends ResponseStatusException {
    public ImpossibleHash(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
