package com.veotani.shortshorts.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LinkCreatedDto {
    @NotNull
    String linkId;

    public LinkCreatedDto(String linkId) {
        this.linkId = linkId;
    }
}
