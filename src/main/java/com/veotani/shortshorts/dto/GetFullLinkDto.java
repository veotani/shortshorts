package com.veotani.shortshorts.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GetFullLinkDto {
    @NotNull
    String linkId;

    public GetFullLinkDto(String shortLinkUrl) {
        this.linkId = shortLinkUrl;
    }
}
