package com.veotani.shortshorts;

import com.veotani.shortshorts.entity.ShortLink;
import com.veotani.shortshorts.repository.LinkRepository;
import com.veotani.shortshorts.service.LinkService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LinkServiceTests {
    @Mock
    private LinkRepository linkRepository;

    @Test
    public void createShortLink_SimpleLink_EqualsToHardcodedMd5Hash() {
        String link = "https://google.com";
        String shortLinkId = "9";
        ShortLink shortLink = new ShortLink(shortLinkId, link);
        Mockito.when(linkRepository.save(Mockito.any(ShortLink.class))).thenReturn(shortLink);
        LinkService service = new LinkService(linkRepository);
        assert service.createShortLink(link).equals(shortLinkId);
        assert !service.createShortLink(link).equals(link);
    }

    @Test
    public void getFullLink_SimpleShortLink_IsValidHash() {
        String link = "https://google.com";
        String shortLinkId = "9";
        ShortLink shortLink = new ShortLink(shortLinkId, link);
        Mockito.when(linkRepository.findById(shortLinkId)).thenReturn(java.util.Optional.of(shortLink));
        LinkService service = new LinkService(linkRepository);
        assert service.getFullLink(shortLinkId).equals(link);
        assert !service.createShortLink(link).equals(link);
    }
}
