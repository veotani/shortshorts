# ShortShorts

ShortShorts — это приложение для создания и хранения коротких ссылок.

## Запуск

Рассмотрим несколько способов сборки и запуска приложения: локально и в контейнере.

### Запуск в локальной среде

Разработка велась на OS Windows, поэтому команды приведены в соответствии 
с полученным опытом.

**Требования**:

- Java 11.0.11
- Docker/Redis

**Подготовка:**
1. Запустить Redis: `docker run -p 6379:6379 redis`
2. Собрать приложение `.\gradlew.bat build -x test`

**Запуск:**

Требуется выполнить команду:
```
java -jar .\build\libs\shortshorts-0.0.1-SNAPSHOT.jar `
  --spring.redis.host=localhost `
  --spring.redis.port=6379 `
  --spring.hezelcast.host=localhost `
  --spring.hezelcast.port=5555 `
  --spring.hezelcast.timeout=100 `
  --spring.throttling.capacity=100 `
  --spring.throttling.duration=1
```

### Запуск в контейнере

В контейнере данный сервис реализован в виде распределенной системы с несколькими
копиями Spring приложения для демонстрации throttling для различных серверов. В
результате запуска будут запущены 3 контейнера на портах 8080, 8081 и 8082.

Команда для запуска:
```
docker compose up -d
```

## Описание системы

Рассмотрим различные аспекты разработанного приложения.

### API

API состоит из трех методов:
- GET /link/{shortUrl} для получения полной ссылке по ключу ее короткой
- POST /link для создания новой короткой ссылки
- GET /{shortUrl} для перенаправления пользователя по короткой ссылке на настоящую

Для документирования API используется Swagger, доступный по адресу /swagger-ui.html
для развернутой системы.

Копия документации развернута по [ссылке](https://app.swaggerhub.com/apis-docs/veotani/Shortshorts/v0)
и доступна для изучения без необходимости развертывания сервиса.

### Throttling

Реализация ограничения количества запросов в секунду выполнялась при помощи Hezelcast
и библиотеки Bucket4j. Выбор Hezelcast связан с удобной интеграцией со стороны
Bucket4j и его возможностью масштабироваться в будущем.

Ограничение реализовано в виде фильтра, который можно применить к некоторым адресам.
Это решение может быть изменено в пользу сервиса, ограничивающего запросы, если
учесть, что такое ограничение является бизнес правилом, а не техническим ограничением.

В текущей реализации фильтр можно распространять и на другие контроллеры.

Взаимодействие с Hezelcast реализовано таким образом, чтобы первый сервис,
который обратится к желаемому адресу, будет ждать появления на нем Hezelcast. Если
он не дождется, то будет запущен свой экземпляр в виде дочернего процесса. Это
необходимо учитывать при развертывании системы. Рекомендуются следующие настройки:
- Для основного сервиса, который должен запускать Hezelcast, выставить время ожидания
существующего сервиса на минимум (например, 100 мс);
- Название хоста должно совпадать с хостом основного сервиса;
- Побочные сервисы должны иметь достаточное время ожидания (например, 50.000 мс).

Переменные окружения, которые задают эти параметры, следующие:
- `HEZELCAST_HOST` - название хоста;
- `HEZELCAST_PORT` - порт Hezelcast;
- `HEZELCAST_TIMEOUT` - время ожидания до попытки запуска собственного Hezelcast (в мс);
- `THROTTLING_CAPACITY` - максимальное количество запросов за время `THROTTLING_DURATION`;
- `THROTTLING_DURATION` - время действия ограничения на запрос.

Такая реализация позволяет запускать систему в одном и в нескольких экземплярах, а также
позволяет разместить экземпляры на разных серверах и синхронизировать нагрузку на них.

### Хранение ссылок и TTL

В качестве СУБД использовалась Redis. Данные в задаче являются тривиальными, поэтому
возможность хранения сложных структур не требовалась. При этом сервис не является продуктовым,
поэтому Redis удовлетворит потребности на MVP стадии. Если дополнить определение задачи
различными факторами, такими как:
- Среднее количество пользователей;
- Частота запросов;
- Длина ссылок;
- Необходимость хранения исторических данных;
- Use Case для коротких ссылок и пр.,

выбор может быть сделан в пользу другой СУБД.

При этом Redis имеет сильное преимущество: Time To Live. Чтобы его установить, необходимо
добавить одну аннотацию. Если возникнет необходимость использования постоянного (persistent)
хранилища, Redis может использоваться в качестве кэша между сервисом и сервером БД.

Конфигурация соединения доступна через переменные среды:
- `REDIS_HOST` - название хоста
- `REDIS_PORT` - порт

### Идемпотентность

Чтобы обеспечить идемпотентность согласно [определению из MDN](https://developer.mozilla.org/en-US/docs/Glossary/Idempotent)
должна оставлять сервер в том же состоянии:

> An HTTP method is idempotent if an identical request can be made once or several times in a row with the same effect while 
leaving the server in the same state.

Чтобы запрос действительно оставлял сервер в том же состоянии, при повторной публикации ссылки
сервер **не** обновляет его TTL. Таким образом запрос на создание ссылки в 9:09 не продлит
жизнь ссылки, созданной в 9:00, до 9:19.

При этом данное определение будет нарушено в любом случае, так как серия из двух запросов
с временным промежутком TTL + eps между ними по определению задачи повторно изменит состояние
сервера (если только не запрещать повторное создание одних и тех же ссылок).
